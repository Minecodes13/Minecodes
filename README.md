## Minecodes Official

I am a hobby programmer and develop Bukkit Plugins, Python scripts, C# apps and more

### Contact

[![Youtube](https://raw.githubusercontent.com/Minecodes/Minecodes/master/img/YoutubeIcon.png)](https://youtube.com/c/Minecodes)

[Gitlab](https://gitlab.com/Minecodes13)

[GitLab group](https://gitlab.com/minecodes-codes)

[Discord](https://discord.gg/QKxt6z3)

[HackTheBox](https://www.hackthebox.eu/home/users/profile/492044)

[TryHackMe](https://tryhackme.com/p/Minecodes)
